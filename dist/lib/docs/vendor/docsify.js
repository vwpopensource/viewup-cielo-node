"use strict";
!function () {
    function e(e) { var t = Object.create(null); return function (n) { var i = r(n) ? n : JSON.stringify(n); return t[i] || (t[i] = e(n)); }; }
    var t = e(function (e) { return e.replace(/([A-Z])/g, function (e) { return "-" + e.toLowerCase(); }); }), n = Object.assign || function (e) {
        for (var t = arguments, n = Object.prototype.hasOwnProperty, r = 1; r < arguments.length; r++) {
            var i = Object(t[r]);
            for (var o in i)
                n.call(i, o) && (e[o] = i[o]);
        }
        return e;
    };
    function r(e) { return "string" == typeof e || "number" == typeof e; }
    function i() { }
    function o(e) { return "function" == typeof e; }
    var a = n({ el: "#app", repo: "", maxLevel: 6, subMaxLevel: 0, loadSidebar: null, loadNavbar: null, homepage: "README.md", coverpage: "", basePath: "", auto2top: !1, name: "", themeColor: "", nameLink: window.location.pathname, autoHeader: !1, executeScript: null, noEmoji: !1, ga: "", ext: ".md", mergeNavbar: !1, formatUpdated: "", externalLinkTarget: "_blank", routerMode: "hash", noCompileLinks: [] }, window.$docsify), s = document.currentScript || [].slice.call(document.getElementsByTagName("script")).filter(function (e) { return /docsify\./.test(e.src); })[0];
    if (s) {
        for (var l in a) {
            var c = s.getAttribute("data-" + t(l));
            r(c) && (a[l] = "" === c || c);
        }
        !0 === a.loadSidebar && (a.loadSidebar = "_sidebar" + a.ext), !0 === a.loadNavbar && (a.loadNavbar = "_navbar" + a.ext), !0 === a.coverpage && (a.coverpage = "_coverpage" + a.ext), !0 === a.repo && (a.repo = ""), !0 === a.name && (a.name = "");
    }
    window.$docsify = a;
    function u(e, t, n, r) {
        void 0 === r && (r = i);
        var o = e._hooks[t], a = function (e) {
            var t = o[e];
            if (e >= o.length)
                r(n);
            else if ("function" == typeof t)
                if (2 === t.length)
                    t(n, function (t) { n = t, a(e + 1); });
                else {
                    var i = t(n);
                    n = void 0 !== i ? i : n, a(e + 1);
                }
            else
                a(e + 1);
        };
        a(0);
    }
    var h = !0, p = h && document.body.clientWidth <= 600, d = h && window.history && window.history.pushState && window.history.replaceState && !navigator.userAgent.match(/((iPod|iPhone|iPad).+\bOS\s+[1-4]\D|WebApps\/.+CFNetwork)/), g = {};
    function f(e, t) {
        if (void 0 === t && (t = !1), "string" == typeof e) {
            if (void 0 !== window.Vue)
                return y(e);
            e = t ? y(e) : g[e] || (g[e] = y(e));
        }
        return e;
    }
    var m = h && document, v = h && m.body, b = h && m.head;
    function y(e, t) { return t ? e.querySelector(t) : m.querySelector(e); }
    function k(e, t) { return [].slice.call(t ? e.querySelectorAll(t) : m.querySelectorAll(e)); }
    function w(e, t) { return e = m.createElement(e), t && (e.innerHTML = t), e; }
    function x(e, t) { return e.appendChild(t); }
    function _(e, t) { return e.insertBefore(t, e.children[0]); }
    function S(e, t, n) { o(t) ? window.addEventListener(e, t) : e.addEventListener(t, n); }
    function C(e, t, n) { o(t) ? window.removeEventListener(e, t) : e.removeEventListener(t, n); }
    function L(e, t, n) { e && e.classList[n ? t : "toggle"](n || t); }
    var E = Object.freeze({ getNode: f, $: m, body: v, head: b, find: y, findAll: k, create: w, appendTo: x, before: _, on: S, off: C, toggleClass: L, style: function (e) { x(b, w("style", e)); } });
    function $(e, t) { return void 0 === t && (t = ""), e && e.length ? (e.forEach(function (e) { t += '<li><a class="section-link" href="' + e.slug + '">' + e.title + "</a></li>", e.children && (t += '<li><ul class="children">' + $(e.children) + "</li></ul>"); }), t) : ""; }
    function A(e, t) { return '<p class="' + e + '">' + t.slice(5).trim() + "</p>"; }
    var T, P;
    function O(e) { var t, n = e.loaded, r = e.total, i = e.step; !T && function () { var e = w("div"); e.classList.add("progress"), x(v, e), T = e; }(), t = i ? (t = parseInt(T.style.width || 0, 10) + i) > 80 ? 80 : t : Math.floor(n / r * 100), T.style.opacity = 1, T.style.width = t >= 95 ? "100%" : t + "%", t >= 95 && (clearTimeout(P), P = setTimeout(function (e) { T.style.opacity = 0, T.style.width = "0%"; }, 200)); }
    var F = {};
    function M(e, t, n) {
        void 0 === t && (t = !1), void 0 === n && (n = {});
        var r = new XMLHttpRequest, o = function () { r.addEventListener.apply(r, arguments); }, a = F[e];
        if (a)
            return { then: function (e) { return e(a.content, a.opt); }, abort: i };
        r.open("GET", e);
        for (var s in n)
            r.setRequestHeader(s, n[s]);
        return r.send(), { then: function (n, a) {
                if (void 0 === a && (a = i), t) {
                    var s = setInterval(function (e) { return O({ step: Math.floor(5 * Math.random() + 1) }); }, 500);
                    o("progress", O), o("loadend", function (e) { O(e), clearInterval(s); });
                }
                o("error", a), o("load", function (t) {
                    var i = t.target;
                    if (i.status >= 400)
                        a(i);
                    else {
                        var o = F[e] = { content: i.response, opt: { updatedAt: r.getResponseHeader("last-modified") } };
                        n(o.content, o.opt);
                    }
                });
            }, abort: function (e) { return 4 !== r.readyState && r.abort(); } };
    }
    function j(e, t) { e.innerHTML = e.innerHTML.replace(/var\(\s*--theme-color.*?\)/g, t); }
    var N = /([^{]*?)\w(?=\})/g, q = { YYYY: "getFullYear", YY: "getYear", MM: function (e) { return e.getMonth() + 1; }, DD: "getDate", HH: "getHours", mm: "getMinutes", ss: "getSeconds" };
    var R = "undefined" != typeof window ? window : "undefined" != typeof global ? global : "undefined" != typeof self ? self : {};
    function H(e, t) { return e(t = { exports: {} }, t.exports), t.exports; }
    var z = H(function (e, t) {
        (function () {
            var t = { newline: /^\n+/, code: /^( {4}[^\n]+\n*)+/, fences: p, hr: /^( *[-*_]){3,} *(?:\n+|$)/, heading: /^ *(#{1,6}) *([^\n]+?) *#* *(?:\n+|$)/, nptable: p, lheading: /^([^\n]+)\n *(=|-){2,} *(?:\n+|$)/, blockquote: /^( *>[^\n]+(\n(?!def)[^\n]+)*\n*)+/, list: /^( *)(bull) [\s\S]+?(?:hr|def|\n{2,}(?! )(?!\1bull )\n*|\s*$)/, html: /^ *(?:comment *(?:\n|\s*$)|closed *(?:\n{2,}|\s*$)|closing *(?:\n{2,}|\s*$))/, def: /^ *\[([^\]]+)\]: *<?([^\s>]+)>?(?: +["(]([^\n]+)[")])? *(?:\n+|$)/, table: p, paragraph: /^((?:[^\n]+\n?(?!hr|heading|lheading|blockquote|tag|def))+)\n*/, text: /^[^\n]+/ };
            t.bullet = /(?:[*+-]|\d+\.)/, t.item = /^( *)(bull) [^\n]*(?:\n(?!\1bull )[^\n]*)*/, t.item = l(t.item, "gm")(/bull/g, t.bullet)(), t.list = l(t.list)(/bull/g, t.bullet)("hr", "\\n+(?=\\1?(?:[-*_] *){3,}(?:\\n+|$))")("def", "\\n+(?=" + t.def.source + ")")(), t.blockquote = l(t.blockquote)("def", t.def)(), t._tag = "(?!(?:a|em|strong|small|s|cite|q|dfn|abbr|data|time|code|var|samp|kbd|sub|sup|i|b|u|mark|ruby|rt|rp|bdi|bdo|span|br|wbr|ins|del|img)\\b)\\w+(?!:/|[^\\w\\s@]*@)\\b", t.html = l(t.html)("comment", /<!--[\s\S]*?-->/)("closed", /<(tag)[\s\S]+?<\/\1>/)("closing", /<tag(?:"[^"]*"|'[^']*'|[^'">])*?>/)(/tag/g, t._tag)(), t.paragraph = l(t.paragraph)("hr", t.hr)("heading", t.heading)("lheading", t.lheading)("blockquote", t.blockquote)("tag", "<" + t._tag)("def", t.def)(), t.normal = d({}, t), t.gfm = d({}, t.normal, { fences: /^ *(`{3,}|~{3,})[ \.]*(\S+)? *\n([\s\S]*?)\s*\1 *(?:\n+|$)/, paragraph: /^/, heading: /^ *(#{1,6}) +([^\n]+?) *#* *(?:\n+|$)/ }), t.gfm.paragraph = l(t.paragraph)("(?!", "(?!" + t.gfm.fences.source.replace("\\1", "\\2") + "|" + t.list.source.replace("\\1", "\\3") + "|")(), t.tables = d({}, t.gfm, { nptable: /^ *(\S.*\|.*)\n *([-:]+ *\|[-| :]*)\n((?:.*\|.*(?:\n|$))*)\n*/, table: /^ *\|(.+)\n *\|( *[-:]+[-| :]*)\n((?: *\|.*(?:\n|$))*)\n*/ });
            function n(e) { this.tokens = [], this.tokens.links = {}, this.options = e || g.defaults, this.rules = t.normal, this.options.gfm && (this.options.tables ? this.rules = t.tables : this.rules = t.gfm); }
            n.rules = t, n.lex = function (e, t) { return new n(t).lex(e); }, n.prototype.lex = function (e) { return e = e.replace(/\r\n|\r/g, "\n").replace(/\t/g, "    ").replace(/\u00a0/g, " ").replace(/\u2424/g, "\n"), this.token(e, !0); }, n.prototype.token = function (e, n, r) {
                var i, o, a, s, l, c, u, h, p;
                for (e = e.replace(/^ +$/gm, ""); e;)
                    if ((a = this.rules.newline.exec(e)) && (e = e.substring(a[0].length), a[0].length > 1 && this.tokens.push({ type: "space" })), a = this.rules.code.exec(e))
                        e = e.substring(a[0].length), a = a[0].replace(/^ {4}/gm, ""), this.tokens.push({ type: "code", text: this.options.pedantic ? a : a.replace(/\n+$/, "") });
                    else if (a = this.rules.fences.exec(e))
                        e = e.substring(a[0].length), this.tokens.push({ type: "code", lang: a[2], text: a[3] || "" });
                    else if (a = this.rules.heading.exec(e))
                        e = e.substring(a[0].length), this.tokens.push({ type: "heading", depth: a[1].length, text: a[2] });
                    else if (n && (a = this.rules.nptable.exec(e))) {
                        for (e = e.substring(a[0].length), c = { type: "table", header: a[1].replace(/^ *| *\| *$/g, "").split(/ *\| */), align: a[2].replace(/^ *|\| *$/g, "").split(/ *\| */), cells: a[3].replace(/\n$/, "").split("\n") }, h = 0; h < c.align.length; h++)
                            /^ *-+: *$/.test(c.align[h]) ? c.align[h] = "right" : /^ *:-+: *$/.test(c.align[h]) ? c.align[h] = "center" : /^ *:-+ *$/.test(c.align[h]) ? c.align[h] = "left" : c.align[h] = null;
                        for (h = 0; h < c.cells.length; h++)
                            c.cells[h] = c.cells[h].split(/ *\| */);
                        this.tokens.push(c);
                    }
                    else if (a = this.rules.lheading.exec(e))
                        e = e.substring(a[0].length), this.tokens.push({ type: "heading", depth: "=" === a[2] ? 1 : 2, text: a[1] });
                    else if (a = this.rules.hr.exec(e))
                        e = e.substring(a[0].length), this.tokens.push({ type: "hr" });
                    else if (a = this.rules.blockquote.exec(e))
                        e = e.substring(a[0].length), this.tokens.push({ type: "blockquote_start" }), a = a[0].replace(/^ *> ?/gm, ""), this.token(a, n, !0), this.tokens.push({ type: "blockquote_end" });
                    else if (a = this.rules.list.exec(e)) {
                        for (e = e.substring(a[0].length), s = a[2], this.tokens.push({ type: "list_start", ordered: s.length > 1 }), i = !1, p = (a = a[0].match(this.rules.item)).length, h = 0; h < p; h++)
                            u = (c = a[h]).length, ~(c = c.replace(/^ *([*+-]|\d+\.) +/, "")).indexOf("\n ") && (u -= c.length, c = this.options.pedantic ? c.replace(/^ {1,4}/gm, "") : c.replace(new RegExp("^ {1," + u + "}", "gm"), "")), this.options.smartLists && h !== p - 1 && (s === (l = t.bullet.exec(a[h + 1])[0]) || s.length > 1 && l.length > 1 || (e = a.slice(h + 1).join("\n") + e, h = p - 1)), o = i || /\n\n(?!\s*$)/.test(c), h !== p - 1 && (i = "\n" === c.charAt(c.length - 1), o || (o = i)), this.tokens.push({ type: o ? "loose_item_start" : "list_item_start" }), this.token(c, !1, r), this.tokens.push({ type: "list_item_end" });
                        this.tokens.push({ type: "list_end" });
                    }
                    else if (a = this.rules.html.exec(e))
                        e = e.substring(a[0].length), this.tokens.push({ type: this.options.sanitize ? "paragraph" : "html", pre: !this.options.sanitizer && ("pre" === a[1] || "script" === a[1] || "style" === a[1]), text: a[0] });
                    else if (!r && n && (a = this.rules.def.exec(e)))
                        e = e.substring(a[0].length), this.tokens.links[a[1].toLowerCase()] = { href: a[2], title: a[3] };
                    else if (n && (a = this.rules.table.exec(e))) {
                        for (e = e.substring(a[0].length), c = { type: "table", header: a[1].replace(/^ *| *\| *$/g, "").split(/ *\| */), align: a[2].replace(/^ *|\| *$/g, "").split(/ *\| */), cells: a[3].replace(/(?: *\| *)?\n$/, "").split("\n") }, h = 0; h < c.align.length; h++)
                            /^ *-+: *$/.test(c.align[h]) ? c.align[h] = "right" : /^ *:-+: *$/.test(c.align[h]) ? c.align[h] = "center" : /^ *:-+ *$/.test(c.align[h]) ? c.align[h] = "left" : c.align[h] = null;
                        for (h = 0; h < c.cells.length; h++)
                            c.cells[h] = c.cells[h].replace(/^ *\| *| *\| *$/g, "").split(/ *\| */);
                        this.tokens.push(c);
                    }
                    else if (n && (a = this.rules.paragraph.exec(e)))
                        e = e.substring(a[0].length), this.tokens.push({ type: "paragraph", text: "\n" === a[1].charAt(a[1].length - 1) ? a[1].slice(0, -1) : a[1] });
                    else if (a = this.rules.text.exec(e))
                        e = e.substring(a[0].length), this.tokens.push({ type: "text", text: a[0] });
                    else if (e)
                        throw new Error("Infinite loop on byte: " + e.charCodeAt(0));
                return this.tokens;
            };
            var r = { escape: /^\\([\\`*{}\[\]()#+\-.!_>])/, autolink: /^<([^ <>]+(@|:\/)[^ <>]+)>/, url: p, tag: /^<!--[\s\S]*?-->|^<\/?\w+(?:"[^"]*"|'[^']*'|[^<'">])*?>/, link: /^!?\[(inside)\]\(href\)/, reflink: /^!?\[(inside)\]\s*\[([^\]]*)\]/, nolink: /^!?\[((?:\[[^\]]*\]|[^\[\]])*)\]/, strong: /^__([\s\S]+?)__(?!_)|^\*\*([\s\S]+?)\*\*(?!\*)/, em: /^\b_((?:[^_]|__)+?)_\b|^\*((?:\*\*|[\s\S])+?)\*(?!\*)/, code: /^(`+)([\s\S]*?[^`])\1(?!`)/, br: /^ {2,}\n(?!\s*$)/, del: p, text: /^[\s\S]+?(?=[\\<!\[_*`]| {2,}\n|$)/ };
            r._inside = /(?:\[[^\]]*\]|\\[\[\]]|[^\[\]]|\](?=[^\[]*\]))*/, r._href = /\s*<?([\s\S]*?)>?(?:\s+['"]([\s\S]*?)['"])?\s*/, r.link = l(r.link)("inside", r._inside)("href", r._href)(), r.reflink = l(r.reflink)("inside", r._inside)(), r.normal = d({}, r), r.pedantic = d({}, r.normal, { strong: /^__(?=\S)([\s\S]*?\S)__(?!_)|^\*\*(?=\S)([\s\S]*?\S)\*\*(?!\*)/, em: /^_(?=\S)([\s\S]*?\S)_(?!_)|^\*(?=\S)([\s\S]*?\S)\*(?!\*)/ }), r.gfm = d({}, r.normal, { escape: l(r.escape)("])", "~|])")(), url: /^(https?:\/\/[^\s<]+[^<.,:;"')\]\s])/, del: /^~~(?=\S)([\s\S]*?\S)~~/, text: l(r.text)("]|", "~]|")("|", "|https?://|")() }), r.breaks = d({}, r.gfm, { br: l(r.br)("{2,}", "*")(), text: l(r.gfm.text)("{2,}", "*")() });
            function i(e, t) {
                if (this.options = t || g.defaults, this.links = e, this.rules = r.normal, this.renderer = this.options.renderer || new o, this.renderer.options = this.options, !this.links)
                    throw new Error("Tokens array requires a `links` property.");
                this.options.gfm ? this.options.breaks ? this.rules = r.breaks : this.rules = r.gfm : this.options.pedantic && (this.rules = r.pedantic);
            }
            i.rules = r, i.output = function (e, t, n) { return new i(t, n).output(e); }, i.prototype.output = function (e) {
                for (var t, n, r, i, o = ""; e;)
                    if (i = this.rules.escape.exec(e))
                        e = e.substring(i[0].length), o += i[1];
                    else if (i = this.rules.autolink.exec(e))
                        e = e.substring(i[0].length), "@" === i[2] ? (n = s(":" === i[1].charAt(6) ? this.mangle(i[1].substring(7)) : this.mangle(i[1])), r = this.mangle("mailto:") + n) : r = n = s(i[1]), o += this.renderer.link(r, null, n);
                    else if (this.inLink || !(i = this.rules.url.exec(e))) {
                        if (i = this.rules.tag.exec(e))
                            !this.inLink && /^<a /i.test(i[0]) ? this.inLink = !0 : this.inLink && /^<\/a>/i.test(i[0]) && (this.inLink = !1), e = e.substring(i[0].length), o += this.options.sanitize ? this.options.sanitizer ? this.options.sanitizer(i[0]) : s(i[0]) : i[0];
                        else if (i = this.rules.link.exec(e))
                            e = e.substring(i[0].length), this.inLink = !0, o += this.outputLink(i, { href: i[2], title: i[3] }), this.inLink = !1;
                        else if ((i = this.rules.reflink.exec(e)) || (i = this.rules.nolink.exec(e))) {
                            if (e = e.substring(i[0].length), t = (i[2] || i[1]).replace(/\s+/g, " "), !(t = this.links[t.toLowerCase()]) || !t.href) {
                                o += i[0].charAt(0), e = i[0].substring(1) + e;
                                continue;
                            }
                            this.inLink = !0, o += this.outputLink(i, t), this.inLink = !1;
                        }
                        else if (i = this.rules.strong.exec(e))
                            e = e.substring(i[0].length), o += this.renderer.strong(this.output(i[2] || i[1]));
                        else if (i = this.rules.em.exec(e))
                            e = e.substring(i[0].length), o += this.renderer.em(this.output(i[2] || i[1]));
                        else if (i = this.rules.code.exec(e))
                            e = e.substring(i[0].length), o += this.renderer.codespan(s(i[2].trim(), !0));
                        else if (i = this.rules.br.exec(e))
                            e = e.substring(i[0].length), o += this.renderer.br();
                        else if (i = this.rules.del.exec(e))
                            e = e.substring(i[0].length), o += this.renderer.del(this.output(i[1]));
                        else if (i = this.rules.text.exec(e))
                            e = e.substring(i[0].length), o += this.renderer.text(s(this.smartypants(i[0])));
                        else if (e)
                            throw new Error("Infinite loop on byte: " + e.charCodeAt(0));
                    }
                    else
                        e = e.substring(i[0].length), r = n = s(i[1]), o += this.renderer.link(r, null, n);
                return o;
            }, i.prototype.outputLink = function (e, t) { var n = s(t.href), r = t.title ? s(t.title) : null; return "!" !== e[0].charAt(0) ? this.renderer.link(n, r, this.output(e[1])) : this.renderer.image(n, r, s(e[1])); }, i.prototype.smartypants = function (e) { return this.options.smartypants ? e.replace(/---/g, "—").replace(/--/g, "–").replace(/(^|[-\u2014/(\[{"\s])'/g, "$1‘").replace(/'/g, "’").replace(/(^|[-\u2014/(\[{\u2018\s])"/g, "$1“").replace(/"/g, "”").replace(/\.{3}/g, "…") : e; }, i.prototype.mangle = function (e) {
                if (!this.options.mangle)
                    return e;
                for (var t, n = "", r = e.length, i = 0; i < r; i++)
                    t = e.charCodeAt(i), Math.random() > .5 && (t = "x" + t.toString(16)), n += "&#" + t + ";";
                return n;
            };
            function o(e) { this.options = e || {}; }
            o.prototype.code = function (e, t, n) {
                if (this.options.highlight) {
                    var r = this.options.highlight(e, t);
                    null != r && r !== e && (n = !0, e = r);
                }
                return t ? '<pre><code class="' + this.options.langPrefix + s(t, !0) + '">' + (n ? e : s(e, !0)) + "\n</code></pre>\n" : "<pre><code>" + (n ? e : s(e, !0)) + "\n</code></pre>";
            }, o.prototype.blockquote = function (e) { return "<blockquote>\n" + e + "</blockquote>\n"; }, o.prototype.html = function (e) { return e; }, o.prototype.heading = function (e, t, n) { return "<h" + t + ' id="' + this.options.headerPrefix + n.toLowerCase().replace(/[^\w]+/g, "-") + '">' + e + "</h" + t + ">\n"; }, o.prototype.hr = function () { return this.options.xhtml ? "<hr/>\n" : "<hr>\n"; }, o.prototype.list = function (e, t) { var n = t ? "ol" : "ul"; return "<" + n + ">\n" + e + "</" + n + ">\n"; }, o.prototype.listitem = function (e) { return "<li>" + e + "</li>\n"; }, o.prototype.paragraph = function (e) { return "<p>" + e + "</p>\n"; }, o.prototype.table = function (e, t) { return "<table>\n<thead>\n" + e + "</thead>\n<tbody>\n" + t + "</tbody>\n</table>\n"; }, o.prototype.tablerow = function (e) { return "<tr>\n" + e + "</tr>\n"; }, o.prototype.tablecell = function (e, t) { var n = t.header ? "th" : "td"; return (t.align ? "<" + n + ' style="text-align:' + t.align + '">' : "<" + n + ">") + e + "</" + n + ">\n"; }, o.prototype.strong = function (e) { return "<strong>" + e + "</strong>"; }, o.prototype.em = function (e) { return "<em>" + e + "</em>"; }, o.prototype.codespan = function (e) { return "<code>" + e + "</code>"; }, o.prototype.br = function () { return this.options.xhtml ? "<br/>" : "<br>"; }, o.prototype.del = function (e) { return "<del>" + e + "</del>"; }, o.prototype.link = function (e, t, n) {
                if (this.options.sanitize) {
                    try {
                        var r = decodeURIComponent((i = e, i.replace(/&(#(?:\d+)|(?:#x[0-9A-Fa-f]+)|(?:\w+));?/gi, function (e, t) { return "colon" === (t = t.toLowerCase()) ? ":" : "#" === t.charAt(0) ? "x" === t.charAt(1) ? String.fromCharCode(parseInt(t.substring(2), 16)) : String.fromCharCode(+t.substring(1)) : ""; }))).replace(/[^\w:]/g, "").toLowerCase();
                    }
                    catch (e) {
                        return n;
                    }
                    if (0 === r.indexOf("javascript:") || 0 === r.indexOf("vbscript:") || 0 === r.indexOf("data:"))
                        return n;
                }
                var i;
                this.options.baseUrl && !h.test(e) && (e = c(this.options.baseUrl, e));
                var o = '<a href="' + e + '"';
                return t && (o += ' title="' + t + '"'), o += ">" + n + "</a>";
            }, o.prototype.image = function (e, t, n) { this.options.baseUrl && !h.test(e) && (e = c(this.options.baseUrl, e)); var r = '<img src="' + e + '" alt="' + n + '"'; return t && (r += ' title="' + t + '"'), r += this.options.xhtml ? "/>" : ">"; }, o.prototype.text = function (e) { return e; };
            function a(e) { this.tokens = [], this.token = null, this.options = e || g.defaults, this.options.renderer = this.options.renderer || new o, this.renderer = this.options.renderer, this.renderer.options = this.options; }
            a.parse = function (e, t, n) { return new a(t, n).parse(e); }, a.prototype.parse = function (e) {
                this.inline = new i(e.links, this.options, this.renderer), this.tokens = e.reverse();
                for (var t = ""; this.next();)
                    t += this.tok();
                return t;
            }, a.prototype.next = function () { return this.token = this.tokens.pop(); }, a.prototype.peek = function () { return this.tokens[this.tokens.length - 1] || 0; }, a.prototype.parseText = function () {
                for (var e = this.token.text; "text" === this.peek().type;)
                    e += "\n" + this.next().text;
                return this.inline.output(e);
            }, a.prototype.tok = function () {
                switch (this.token.type) {
                    case "space": return "";
                    case "hr": return this.renderer.hr();
                    case "heading": return this.renderer.heading(this.inline.output(this.token.text), this.token.depth, this.token.text);
                    case "code": return this.renderer.code(this.token.text, this.token.lang, this.token.escaped);
                    case "table":
                        var e, t, n, r, i = "", o = "";
                        for (n = "", e = 0; e < this.token.header.length; e++)
                            ({ header: !0, align: this.token.align[e] }), n += this.renderer.tablecell(this.inline.output(this.token.header[e]), { header: !0, align: this.token.align[e] });
                        for (i += this.renderer.tablerow(n), e = 0; e < this.token.cells.length; e++) {
                            for (t = this.token.cells[e], n = "", r = 0; r < t.length; r++)
                                n += this.renderer.tablecell(this.inline.output(t[r]), { header: !1, align: this.token.align[r] });
                            o += this.renderer.tablerow(n);
                        }
                        return this.renderer.table(i, o);
                    case "blockquote_start":
                        for (o = ""; "blockquote_end" !== this.next().type;)
                            o += this.tok();
                        return this.renderer.blockquote(o);
                    case "list_start":
                        o = "";
                        for (var a = this.token.ordered; "list_end" !== this.next().type;)
                            o += this.tok();
                        return this.renderer.list(o, a);
                    case "list_item_start":
                        for (o = ""; "list_item_end" !== this.next().type;)
                            o += "text" === this.token.type ? this.parseText() : this.tok();
                        return this.renderer.listitem(o);
                    case "loose_item_start":
                        for (o = ""; "list_item_end" !== this.next().type;)
                            o += this.tok();
                        return this.renderer.listitem(o);
                    case "html":
                        var s = this.token.pre || this.options.pedantic ? this.token.text : this.inline.output(this.token.text);
                        return this.renderer.html(s);
                    case "paragraph": return this.renderer.paragraph(this.inline.output(this.token.text));
                    case "text": return this.renderer.paragraph(this.parseText());
                }
            };
            function s(e, t) { return e.replace(t ? /&/g : /&(?!#?\w+;)/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;").replace(/'/g, "&#39;"); }
            function l(e, t) { return e = e.source, t = t || "", function n(r, i) { return r ? (i = (i = i.source || i).replace(/(^|[^\[])\^/g, "$1"), e = e.replace(r, i), n) : new RegExp(e, t); }; }
            function c(e, t) { return u[" " + e] || (/^[^:]+:\/*[^/]*$/.test(e) ? u[" " + e] = e + "/" : u[" " + e] = e.replace(/[^/]*$/, "")), e = u[" " + e], "//" === t.slice(0, 2) ? e.replace(/:[\s\S]*/, ":") + t : "/" === t.charAt(0) ? e.replace(/(:\/*[^/]*)[\s\S]*/, "$1") + t : e + t; }
            var u = {}, h = /^$|^[a-z][a-z0-9+.-]*:|^[?#]/i;
            function p() { }
            p.exec = p;
            function d(e) {
                for (var t, n, r = arguments, i = 1; i < arguments.length; i++) {
                    t = r[i];
                    for (n in t)
                        Object.prototype.hasOwnProperty.call(t, n) && (e[n] = t[n]);
                }
                return e;
            }
            function g(e, t, r) {
                if (r || "function" == typeof t) {
                    r || (r = t, t = null);
                    var i, o, l = (t = d({}, g.defaults, t || {})).highlight, c = 0;
                    try {
                        i = n.lex(e, t);
                    }
                    catch (e) {
                        return r(e);
                    }
                    o = i.length;
                    var u = function (e) {
                        if (e)
                            return t.highlight = l, r(e);
                        var n;
                        try {
                            n = a.parse(i, t);
                        }
                        catch (t) {
                            e = t;
                        }
                        return t.highlight = l, e ? r(e) : r(null, n);
                    };
                    if (!l || l.length < 3)
                        return u();
                    if (delete t.highlight, !o)
                        return u();
                    for (; c < i.length; c++)
                        !function (e) { "code" !== e.type ? --o || u() : l(e.text, e.lang, function (t, n) { return t ? u(t) : null == n || n === e.text ? --o || u() : (e.text = n, e.escaped = !0, void (--o || u())); }); }(i[c]);
                }
                else
                    try {
                        return t && (t = d({}, g.defaults, t)), a.parse(n.lex(e, t), t);
                    }
                    catch (e) {
                        if (e.message += "\nPlease report this to https://github.com/chjj/marked.", (t || g.defaults).silent)
                            return "<p>An error occurred:</p><pre>" + s(e.message + "", !0) + "</pre>";
                        throw e;
                    }
            }
            g.options = g.setOptions = function (e) { return d(g.defaults, e), g; }, g.defaults = { gfm: !0, tables: !0, breaks: !1, pedantic: !1, sanitize: !1, sanitizer: null, mangle: !0, smartLists: !1, silent: !1, highlight: null, langPrefix: "lang-", smartypants: !1, headerPrefix: "", renderer: new o, xhtml: !1, baseUrl: null }, g.Parser = a, g.parser = a.parse, g.Renderer = o, g.Lexer = n, g.lexer = n.lex, g.InlineLexer = i, g.inlineLexer = i.output, g.parse = g, e.exports = g;
        }).call(function () { return this || ("undefined" != typeof window ? window : R); }());
    }), I = H(function (e) {
        var t = "undefined" != typeof window ? window : "undefined" != typeof WorkerGlobalScope && self instanceof WorkerGlobalScope ? self : {}, n = function () {
            var e = /\blang(?:uage)?-(\w+)\b/i, n = 0, r = t.Prism = { manual: t.Prism && t.Prism.manual, disableWorkerMessageHandler: t.Prism && t.Prism.disableWorkerMessageHandler, util: { encode: function (e) { return e instanceof i ? new i(e.type, r.util.encode(e.content), e.alias) : "Array" === r.util.type(e) ? e.map(r.util.encode) : e.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/\u00a0/g, " "); }, type: function (e) { return Object.prototype.toString.call(e).match(/\[object (\w+)\]/)[1]; }, objId: function (e) { return e.__id || Object.defineProperty(e, "__id", { value: ++n }), e.__id; }, clone: function (e) {
                        switch (r.util.type(e)) {
                            case "Object":
                                var t = {};
                                for (var n in e)
                                    e.hasOwnProperty(n) && (t[n] = r.util.clone(e[n]));
                                return t;
                            case "Array": return e.map(function (e) { return r.util.clone(e); });
                        }
                        return e;
                    } }, languages: { extend: function (e, t) {
                        var n = r.util.clone(r.languages[e]);
                        for (var i in t)
                            n[i] = t[i];
                        return n;
                    }, insertBefore: function (e, t, n, i) {
                        var o = (i = i || r.languages)[e];
                        if (2 == arguments.length) {
                            n = arguments[1];
                            for (var a in n)
                                n.hasOwnProperty(a) && (o[a] = n[a]);
                            return o;
                        }
                        var s = {};
                        for (var l in o)
                            if (o.hasOwnProperty(l)) {
                                if (l == t)
                                    for (var a in n)
                                        n.hasOwnProperty(a) && (s[a] = n[a]);
                                s[l] = o[l];
                            }
                        return r.languages.DFS(r.languages, function (t, n) { n === i[e] && t != e && (this[t] = s); }), i[e] = s;
                    }, DFS: function (e, t, n, i) {
                        i = i || {};
                        for (var o in e)
                            e.hasOwnProperty(o) && (t.call(e, o, e[o], n || o), "Object" !== r.util.type(e[o]) || i[r.util.objId(e[o])] ? "Array" !== r.util.type(e[o]) || i[r.util.objId(e[o])] || (i[r.util.objId(e[o])] = !0, r.languages.DFS(e[o], t, o, i)) : (i[r.util.objId(e[o])] = !0, r.languages.DFS(e[o], t, null, i)));
                    } }, plugins: {}, highlightAll: function (e, t) { r.highlightAllUnder(document, e, t); }, highlightAllUnder: function (e, t, n) {
                    var i = { callback: n, selector: 'code[class*="language-"], [class*="language-"] code, code[class*="lang-"], [class*="lang-"] code' };
                    r.hooks.run("before-highlightall", i);
                    for (var o, a = i.elements || e.querySelectorAll(i.selector), s = 0; o = a[s++];)
                        r.highlightElement(o, !0 === t, i.callback);
                }, highlightElement: function (n, i, o) {
                    for (var a, s, l = n; l && !e.test(l.className);)
                        l = l.parentNode;
                    l && (a = (l.className.match(e) || [, ""])[1].toLowerCase(), s = r.languages[a]), n.className = n.className.replace(e, "").replace(/\s+/g, " ") + " language-" + a, n.parentNode && (l = n.parentNode, /pre/i.test(l.nodeName) && (l.className = l.className.replace(e, "").replace(/\s+/g, " ") + " language-" + a));
                    var c = { element: n, language: a, grammar: s, code: n.textContent };
                    if (r.hooks.run("before-sanity-check", c), !c.code || !c.grammar)
                        return c.code && (r.hooks.run("before-highlight", c), c.element.textContent = c.code, r.hooks.run("after-highlight", c)), void r.hooks.run("complete", c);
                    if (r.hooks.run("before-highlight", c), i && t.Worker) {
                        var u = new Worker(r.filename);
                        u.onmessage = function (e) { c.highlightedCode = e.data, r.hooks.run("before-insert", c), c.element.innerHTML = c.highlightedCode, o && o.call(c.element), r.hooks.run("after-highlight", c), r.hooks.run("complete", c); }, u.postMessage(JSON.stringify({ language: c.language, code: c.code, immediateClose: !0 }));
                    }
                    else
                        c.highlightedCode = r.highlight(c.code, c.grammar, c.language), r.hooks.run("before-insert", c), c.element.innerHTML = c.highlightedCode, o && o.call(n), r.hooks.run("after-highlight", c), r.hooks.run("complete", c);
                }, highlight: function (e, t, n) { var o = r.tokenize(e, t); return i.stringify(r.util.encode(o), n); }, matchGrammar: function (e, t, n, i, o, a, s) {
                    var l = r.Token;
                    for (var c in n)
                        if (n.hasOwnProperty(c) && n[c]) {
                            if (c == s)
                                return;
                            var u = n[c];
                            u = "Array" === r.util.type(u) ? u : [u];
                            for (var h = 0; h < u.length; ++h) {
                                var p = u[h], d = p.inside, g = !!p.lookbehind, f = !!p.greedy, m = 0, v = p.alias;
                                if (f && !p.pattern.global) {
                                    var b = p.pattern.toString().match(/[imuy]*$/)[0];
                                    p.pattern = RegExp(p.pattern.source, b + "g");
                                }
                                p = p.pattern || p;
                                for (var y = i, k = o; y < t.length; k += t[y].length, ++y) {
                                    var w = t[y];
                                    if (t.length > e.length)
                                        return;
                                    if (!(w instanceof l)) {
                                        p.lastIndex = 0;
                                        var x = 1;
                                        if (!($ = p.exec(w)) && f && y != t.length - 1) {
                                            if (p.lastIndex = k, !($ = p.exec(e)))
                                                break;
                                            for (var _ = $.index + (g ? $[1].length : 0), S = $.index + $[0].length, C = y, L = k, E = t.length; C < E && (L < S || !t[C].type && !t[C - 1].greedy); ++C)
                                                _ >= (L += t[C].length) && (++y, k = L);
                                            if (t[y] instanceof l || t[C - 1].greedy)
                                                continue;
                                            x = C - y, w = e.slice(k, L), $.index -= k;
                                        }
                                        if ($) {
                                            g && (m = $[1].length);
                                            S = (_ = $.index + m) + ($ = $[0].slice(m)).length;
                                            var $, A = w.slice(0, _), T = w.slice(S), P = [y, x];
                                            A && (++y, k += A.length, P.push(A));
                                            var O = new l(c, d ? r.tokenize($, d) : $, v, $, f);
                                            if (P.push(O), T && P.push(T), Array.prototype.splice.apply(t, P), 1 != x && r.matchGrammar(e, t, n, y, k, !0, c), a)
                                                break;
                                        }
                                        else if (a)
                                            break;
                                    }
                                }
                            }
                        }
                }, tokenize: function (e, t, n) {
                    var i = [e], o = t.rest;
                    if (o) {
                        for (var a in o)
                            t[a] = o[a];
                        delete t.rest;
                    }
                    return r.matchGrammar(e, i, t, 0, 0, !1), i;
                }, hooks: { all: {}, add: function (e, t) { var n = r.hooks.all; n[e] = n[e] || [], n[e].push(t); }, run: function (e, t) {
                        var n = r.hooks.all[e];
                        if (n && n.length)
                            for (var i, o = 0; i = n[o++];)
                                i(t);
                    } } }, i = r.Token = function (e, t, n, r, i) { this.type = e, this.content = t, this.alias = n, this.length = 0 | (r || "").length, this.greedy = !!i; };
            if (i.stringify = function (e, t, n) {
                if ("string" == typeof e)
                    return e;
                if ("Array" === r.util.type(e))
                    return e.map(function (n) { return i.stringify(n, t, e); }).join("");
                var o = { type: e.type, content: i.stringify(e.content, t, n), tag: "span", classes: ["token", e.type], attributes: {}, language: t, parent: n };
                if (e.alias) {
                    var a = "Array" === r.util.type(e.alias) ? e.alias : [e.alias];
                    Array.prototype.push.apply(o.classes, a);
                }
                r.hooks.run("wrap", o);
                var s = Object.keys(o.attributes).map(function (e) { return e + '="' + (o.attributes[e] || "").replace(/"/g, "&quot;") + '"'; }).join(" ");
                return "<" + o.tag + ' class="' + o.classes.join(" ") + '"' + (s ? " " + s : "") + ">" + o.content + "</" + o.tag + ">";
            }, !t.document)
                return t.addEventListener ? (r.disableWorkerMessageHandler || t.addEventListener("message", function (e) { var n = JSON.parse(e.data), i = n.language, o = n.code, a = n.immediateClose; t.postMessage(r.highlight(o, r.languages[i], i)), a && t.close(); }, !1), t.Prism) : t.Prism;
            var o = document.currentScript || [].slice.call(document.getElementsByTagName("script")).pop();
            return o && (r.filename = o.src, r.manual || o.hasAttribute("data-manual") || ("loading" !== document.readyState ? window.requestAnimationFrame ? window.requestAnimationFrame(r.highlightAll) : window.setTimeout(r.highlightAll, 16) : document.addEventListener("DOMContentLoaded", r.highlightAll))), t.Prism;
        }();
        e.exports && (e.exports = n), void 0 !== R && (R.Prism = n), n.languages.markup = { comment: /<!--[\s\S]*?-->/, prolog: /<\?[\s\S]+?\?>/, doctype: /<!DOCTYPE[\s\S]+?>/i, cdata: /<!\[CDATA\[[\s\S]*?]]>/i, tag: { pattern: /<\/?(?!\d)[^\s>\/=$<]+(?:\s+[^\s>\/=]+(?:=(?:("|')(?:\\[\s\S]|(?!\1)[^\\])*\1|[^\s'">=]+))?)*\s*\/?>/i, inside: { tag: { pattern: /^<\/?[^\s>\/]+/i, inside: { punctuation: /^<\/?/, namespace: /^[^\s>\/:]+:/ } }, "attr-value": { pattern: /=(?:("|')(?:\\[\s\S]|(?!\1)[^\\])*\1|[^\s'">=]+)/i, inside: { punctuation: [/^=/, { pattern: /(^|[^\\])["']/, lookbehind: !0 }] } }, punctuation: /\/?>/, "attr-name": { pattern: /[^\s>\/]+/, inside: { namespace: /^[^\s>\/:]+:/ } } } }, entity: /&#?[\da-z]{1,8};/i }, n.languages.markup.tag.inside["attr-value"].inside.entity = n.languages.markup.entity, n.hooks.add("wrap", function (e) { "entity" === e.type && (e.attributes.title = e.content.replace(/&amp;/, "&")); }), n.languages.xml = n.languages.markup, n.languages.html = n.languages.markup, n.languages.mathml = n.languages.markup, n.languages.svg = n.languages.markup, n.languages.css = { comment: /\/\*[\s\S]*?\*\//, atrule: { pattern: /@[\w-]+?.*?(?:;|(?=\s*\{))/i, inside: { rule: /@[\w-]+/ } }, url: /url\((?:(["'])(?:\\(?:\r\n|[\s\S])|(?!\1)[^\\\r\n])*\1|.*?)\)/i, selector: /[^{}\s][^{};]*?(?=\s*\{)/, string: { pattern: /("|')(?:\\(?:\r\n|[\s\S])|(?!\1)[^\\\r\n])*\1/, greedy: !0 }, property: /[-_a-z\xA0-\uFFFF][-\w\xA0-\uFFFF]*(?=\s*:)/i, important: /\B!important\b/i, function: /[-a-z0-9]+(?=\()/i, punctuation: /[(){};:]/ }, n.languages.css.atrule.inside.rest = n.util.clone(n.languages.css), n.languages.markup && (n.languages.insertBefore("markup", "tag", { style: { pattern: /(<style[\s\S]*?>)[\s\S]*?(?=<\/style>)/i, lookbehind: !0, inside: n.languages.css, alias: "language-css", greedy: !0 } }), n.languages.insertBefore("inside", "attr-value", { "style-attr": { pattern: /\s*style=("|')(?:\\[\s\S]|(?!\1)[^\\])*\1/i, inside: { "attr-name": { pattern: /^\s*style/i, inside: n.languages.markup.tag.inside }, punctuation: /^\s*=\s*['"]|['"]\s*$/, "attr-value": { pattern: /.+/i, inside: n.languages.css } }, alias: "language-css" } }, n.languages.markup.tag)), n.languages.clike = { comment: [{ pattern: /(^|[^\\])\/\*[\s\S]*?(?:\*\/|$)/, lookbehind: !0 }, { pattern: /(^|[^\\:])\/\/.*/, lookbehind: !0 }], string: { pattern: /(["'])(?:\\(?:\r\n|[\s\S])|(?!\1)[^\\\r\n])*\1/, greedy: !0 }, "class-name": { pattern: /((?:\b(?:class|interface|extends|implements|trait|instanceof|new)\s+)|(?:catch\s+\())[\w.\\]+/i, lookbehind: !0, inside: { punctuation: /[.\\]/ } }, keyword: /\b(?:if|else|while|do|for|return|in|instanceof|function|new|try|throw|catch|finally|null|break|continue)\b/, boolean: /\b(?:true|false)\b/, function: /[a-z0-9_]+(?=\()/i, number: /\b-?(?:0x[\da-f]+|\d*\.?\d+(?:e[+-]?\d+)?)\b/i, operator: /--?|\+\+?|!=?=?|<=?|>=?|==?=?|&&?|\|\|?|\?|\*|\/|~|\^|%/, punctuation: /[{}[\];(),.:]/ }, n.languages.javascript = n.languages.extend("clike", { keyword: /\b(?:as|async|await|break|case|catch|class|const|continue|debugger|default|delete|do|else|enum|export|extends|finally|for|from|function|get|if|implements|import|in|instanceof|interface|let|new|null|of|package|private|protected|public|return|set|static|super|switch|this|throw|try|typeof|var|void|while|with|yield)\b/, number: /\b-?(?:0[xX][\dA-Fa-f]+|0[bB][01]+|0[oO][0-7]+|\d*\.?\d+(?:[Ee][+-]?\d+)?|NaN|Infinity)\b/, function: /[_$a-z\xA0-\uFFFF][$\w\xA0-\uFFFF]*(?=\s*\()/i, operator: /-[-=]?|\+[+=]?|!=?=?|<<?=?|>>?>?=?|=(?:==?|>)?|&[&=]?|\|[|=]?|\*\*?=?|\/=?|~|\^=?|%=?|\?|\.{3}/ }), n.languages.insertBefore("javascript", "keyword", { regex: { pattern: /(^|[^/])\/(?!\/)(\[[^\]\r\n]+]|\\.|[^/\\\[\r\n])+\/[gimyu]{0,5}(?=\s*($|[\r\n,.;})]))/, lookbehind: !0, greedy: !0 }, "function-variable": { pattern: /[_$a-z\xA0-\uFFFF][$\w\xA0-\uFFFF]*(?=\s*=\s*(?:function\b|(?:\([^()]*\)|[_$a-z\xA0-\uFFFF][$\w\xA0-\uFFFF]*)\s*=>))/i, alias: "function" } }), n.languages.insertBefore("javascript", "string", { "template-string": { pattern: /`(?:\\[\s\S]|[^\\`])*`/, greedy: !0, inside: { interpolation: { pattern: /\$\{[^}]+\}/, inside: { "interpolation-punctuation": { pattern: /^\$\{|\}$/, alias: "punctuation" }, rest: n.languages.javascript } }, string: /[\s\S]+/ } } }), n.languages.markup && n.languages.insertBefore("markup", "tag", { script: { pattern: /(<script[\s\S]*?>)[\s\S]*?(?=<\/script>)/i, lookbehind: !0, inside: n.languages.javascript, alias: "language-javascript", greedy: !0 } }), n.languages.js = n.languages.javascript, "undefined" != typeof self && self.Prism && self.document && document.querySelector && (self.Prism.fileHighlight = function () {
            var e = { js: "javascript", py: "python", rb: "ruby", ps1: "powershell", psm1: "powershell", sh: "bash", bat: "batch", h: "c", tex: "latex" };
            Array.prototype.slice.call(document.querySelectorAll("pre[data-src]")).forEach(function (t) {
                for (var r, i = t.getAttribute("data-src"), o = t, a = /\blang(?:uage)?-(?!\*)(\w+)\b/i; o && !a.test(o.className);)
                    o = o.parentNode;
                if (o && (r = (t.className.match(a) || [, ""])[1]), !r) {
                    var s = (i.match(/\.(\w+)$/) || [, ""])[1];
                    r = e[s] || s;
                }
                var l = document.createElement("code");
                l.className = "language-" + r, t.textContent = "", l.textContent = "Loading…", t.appendChild(l);
                var c = new XMLHttpRequest;
                c.open("GET", i, !0), c.onreadystatechange = function () { 4 == c.readyState && (c.status < 400 && c.responseText ? (l.textContent = c.responseText, n.highlightElement(l)) : c.status >= 400 ? l.textContent = "✖ Error " + c.status + " while fetching file: " + c.statusText : l.textContent = "✖ Error: File does not exist or is empty"); }, c.send(null);
            });
        }, document.addEventListener("DOMContentLoaded", self.Prism.fileHighlight));
    });
    function U(e, t) { var n = [], r = {}; return e.forEach(function (e) { var i = e.level || 1, o = i - 1; i > t || (r[o] ? r[o].children = (r[o].children || []).concat(e) : n.push(e), r[i] = e); }), n; }
    var B = {}, D = /[\u2000-\u206F\u2E00-\u2E7F\\'!"#$%&()*+,.\/:;<=>?@\[\]^`{|}~]/g;
    function Y(e) { return e.toLowerCase(); }
    function W(e) {
        if ("string" != typeof e)
            return "";
        var t = e.trim().replace(/[A-Z]+/g, Y).replace(/<[^>\d]+>/g, "").replace(D, "").replace(/\s/g, "-").replace(/-+/g, "-").replace(/^(\d)/, "_$1"), n = B[t];
        return n = B.hasOwnProperty(t) ? n + 1 : 0, B[t] = n, n && (t = t + "-" + n), t;
    }
    W.clear = function () { B = {}; };
    function G(e, t) { return '<img class="emoji" src="https://assets-cdn.github.com/images/icons/emoji/' + t + '.png" alt="' + t + '" />'; }
    var X = decodeURIComponent, Q = encodeURIComponent;
    function V(e) { var t = {}; return (e = e.trim().replace(/^(\?|#|&)/, "")) ? (e.split("&").forEach(function (e) { var n = e.replace(/\+/g, " ").split("="); t[n[0]] = n[1] && X(n[1]); }), t) : t; }
    function Z(e, t) {
        void 0 === t && (t = []);
        var n = [];
        for (var r in e)
            t.indexOf(r) > -1 || n.push(e[r] ? (Q(r) + "=" + Q(e[r])).toLowerCase() : Q(r));
        return n.length ? "?" + n.join("&") : "";
    }
    function J() {
        for (var e = [], t = arguments.length; t--;)
            e[t] = arguments[t];
        return te(e.join("/"));
    }
    var K = e(function (e) { return /(:|(\/{2}))/g.test(e); }), ee = e(function (e) { return /\/$/g.test(e) ? e : (e = e.match(/(\S*\/)[^\/]+$/)) ? e[1] : ""; }), te = e(function (e) { return e.replace(/^\/+/, "/").replace(/([^:])\/{2,}/g, "$1/"); }), ne = e(function (e) { return e.replace("#", "?id="); }), re = {};
    function ie(e) { void 0 === e && (e = ""); var t = {}; return e && (e = e.replace(/:([\w-]+)=?([\w-]+)?/g, function (e, n, r) { return t[n] = r && r.replace(/&quot;/g, "") || !0, ""; }).trim()), { str: e, config: t }; }
    var oe = { markdown: function (e) { return { url: e }; }, iframe: function (e, t) { return { code: '<iframe src="' + e + '" ' + (t || "width=100% height=400") + "></iframe>" }; }, video: function (e, t) { return { code: '<video src="' + e + '" ' + (t || "controls") + ">Not Support</video>" }; }, audio: function (e, t) { return { code: '<audio src="' + e + '" ' + (t || "controls") + ">Not Support</audio>" }; }, code: function (e, t) { var n = e.match(/\.(\w+)$/); return "md" === (n = t || n && n[1]) && (n = "markdown"), { url: e, lang: n }; } }, ae = function (t, i) {
        this.config = t, this.router = i, this.cacheTree = {}, this.toc = [], this.linkTarget = t.externalLinkTarget || "_blank", this.contentBase = i.getBasePath();
        var a, s = this._initRenderer(), l = t.markdown || {};
        o(l) ? a = l(z, s) : (z.setOptions(n(l, { renderer: n(s, l.renderer) })), a = z), this._marked = a, this.compile = e(function (e) {
            var n = "";
            if (!e)
                return e;
            n = r(e) ? a(e) : a.parser(e), n = t.noEmoji ? n : (i = n, i.replace(/<(pre|template|code)[^>]*?>[\s\S]+?<\/(pre|template|code)>/g, function (e) { return e.replace(/:/g, "__colon__"); }).replace(/:(\w+?):/gi, h && window.emojify || G).replace(/__colon__/g, ":"));
            var i;
            return W.clear(), n;
        });
    };
    ae.prototype.compileEmbed = function (e, t) {
        var n, r = ie(t), i = r.str, o = r.config;
        if (t = i, o.include) {
            K(e) || (e = J(this.contentBase, e));
            var a;
            if (o.type && (a = oe[o.type]))
                (n = a.call(this, e, t)).type = o.type;
            else {
                var s = "code";
                /\.(md|markdown)/.test(e) ? s = "markdown" : /\.html?/.test(e) ? s = "iframe" : /\.(mp4|ogg)/.test(e) ? s = "video" : /\.mp3/.test(e) && (s = "audio"), (n = oe[s].call(this, e, t)).type = s;
            }
            return n;
        }
    }, ae.prototype._matchNotCompileLink = function (e) {
        for (var t = this.config.noCompileLinks || [], n = 0; n < t.length; n++) {
            var r = t[n];
            if ((re[r] || (re[r] = new RegExp("^" + r + "$"))).test(e))
                return e;
        }
    }, ae.prototype._initRenderer = function () { var e = new z.Renderer, t = this.linkTarget, n = this.router, r = this.contentBase, i = this, o = {}; o.heading = e.heading = function (e, t) { var r = { level: t, title: e }; /{docsify-ignore}/g.test(e) && (e = e.replace("{docsify-ignore}", ""), r.title = e, r.ignoreSubHeading = !0), /{docsify-ignore-all}/g.test(e) && (e = e.replace("{docsify-ignore-all}", ""), r.title = e, r.ignoreAllSubs = !0); var o = W(e), a = n.toURL(n.getCurrentPath(), { id: o }); return r.slug = a, i.toc.push(r), "<h" + t + ' id="' + o + '"><a href="' + a + '" data-id="' + o + '" class="anchor"><span>' + e + "</span></a></h" + t + ">"; }, o.code = e.code = function (e, t) { void 0 === t && (t = ""), e = e.replace(/@DOCSIFY_QM@/g, "`"); return '<pre v-pre data-lang="' + t + '"><code class="lang-' + t + '">' + I.highlight(e, I.languages[t] || I.languages.markup) + "</code></pre>"; }, o.link = e.link = function (e, r, o) { void 0 === r && (r = ""); var a = "", s = ie(r), l = s.str, c = s.config; return r = l, /:|(\/{2})/.test(e) || i._matchNotCompileLink(e) || c.ignore ? a += ' target="' + t + '"' : (e === i.config.homepage && (e = "README"), e = n.toURL(e, null, n.getCurrentPath())), c.target && (a += " target=" + c.target), c.disabled && (a += " disabled", e = "javascript:void(0)"), r && (a += ' title="' + r + '"'), '<a href="' + e + '"' + a + ">" + o + "</a>"; }, o.paragraph = e.paragraph = function (e) { return /^!&gt;/.test(e) ? A("tip", e) : /^\?&gt;/.test(e) ? A("warn", e) : "<p>" + e + "</p>"; }, o.image = e.image = function (e, t, n) { var i = e, o = "", a = ie(t); return t = a.str, a.config["no-zoom"] && (o += " data-no-zoom"), t && (o += ' title="' + t + '"'), K(e) || (i = J(r, e)), '<img src="' + i + '"data-origin="' + e + '" alt="' + n + '"' + o + ">"; }; var a = /^\[([ x])\] +/; return o.listitem = e.listitem = function (e) { var t = a.exec(e); return t && (e = e.replace(a, '<input type="checkbox" ' + ("x" === t[1] ? "checked" : "") + " />")), "<li" + (t ? ' class="task-list-item"' : "") + ">" + e + "</li>\n"; }, e.origin = o, e; }, ae.prototype.sidebar = function (e, t) {
        var n = this.router.getCurrentPath(), r = "";
        if (e)
            r = (r = this.compile(e)) && r.match(/<ul[^>]*>([\s\S]+)<\/ul>/g)[0];
        else {
            var i = this.cacheTree[n] || U(this.toc, t);
            r = $(i, "<ul>"), this.cacheTree[n] = i;
        }
        return r;
    }, ae.prototype.subSidebar = function (e) {
        if (e) {
            var t = this.router.getCurrentPath(), n = this.cacheTree, r = this.toc;
            r[0] && r[0].ignoreAllSubs && r.splice(0), r[0] && 1 === r[0].level && r.shift();
            for (var i = 0; i < r.length; i++)
                r[i].ignoreSubHeading && r.splice(i, 1) && i--;
            var o = n[t] || U(r, e);
            return n[t] = o, this.toc = [], $(o, '<ul class="app-sub-sidebar">');
        }
        this.toc = [];
    }, ae.prototype.article = function (e) { return this.compile(e); }, ae.prototype.cover = function (e) { var t = this.toc.slice(), n = this.compile(e); return this.toc = t.slice(), n; };
    var se = m.title;
    function le() {
        var e = f("section.cover");
        if (e) {
            var t = e.getBoundingClientRect().height;
            window.pageYOffset >= t || e.classList.contains("hidden") ? L(v, "add", "sticky") : L(v, "remove", "sticky");
        }
    }
    function ce(e, t, n, r) { var i, o = k(t = f(t), "a"), a = decodeURI(e.toURL(e.getCurrentPath())); return o.sort(function (e, t) { return t.href.length - e.href.length; }).forEach(function (e) { var t = e.getAttribute("href"), r = n ? e.parentNode : e; 0 !== a.indexOf(t) || i ? L(r, "remove", "active") : (i = e, L(r, "add", "active")); }), r && (m.title = i ? i.innerText + " - " + se : se), i; }
    var ue = function () {
        function e(e, t) {
            for (var n = 0; n < t.length; n++) {
                var r = t[n];
                r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r);
            }
        }
        return function (t, n, r) { return n && e(t.prototype, n), r && e(t, r), t; };
    }();
    var he = function () {
        function e() {
            var t = arguments.length > 0 && void 0 !== arguments[0] ? arguments[0] : {};
            !function (e, t) {
                if (!(e instanceof t))
                    throw new TypeError("Cannot call a class as a function");
            }(this, e), this.duration = t.duration || 1e3, this.ease = t.easing || this._defaultEase, this.start = t.start, this.end = t.end, this.frame = null, this.next = null, this.isRunning = !1, this.events = {}, this.direction = this.start < this.end ? "up" : "down";
        }
        return ue(e, [{ key: "begin", value: function () { return this.isRunning || this.next === this.end || (this.frame = window.requestAnimationFrame(this._tick.bind(this))), this; } }, { key: "stop", value: function () { return window.cancelAnimationFrame(this.frame), this.isRunning = !1, this.frame = null, this.timeStart = null, this.next = null, this; } }, { key: "on", value: function (e, t) { return this.events[e] = this.events[e] || [], this.events[e].push(t), this; } }, { key: "emit", value: function (e, t) { var n = this, r = this.events[e]; r && r.forEach(function (e) { return e.call(n, t); }); } }, { key: "_tick", value: function (e) { this.isRunning = !0; var t = this.next || this.start; this.timeStart || (this.timeStart = e), this.timeElapsed = e - this.timeStart, this.next = Math.round(this.ease(this.timeElapsed, this.start, this.end - this.start, this.duration)), this._shouldTick(t) ? (this.emit("tick", this.next), this.frame = window.requestAnimationFrame(this._tick.bind(this))) : (this.emit("tick", this.end), this.emit("done", null)); } }, { key: "_shouldTick", value: function (e) { return { up: this.next < this.end && e <= this.next, down: this.next > this.end && e >= this.next }[this.direction]; } }, { key: "_defaultEase", value: function (e, t, n, r) { return (e /= r / 2) < 1 ? n / 2 * e * e + t : -n / 2 * (--e * (e - 2) - 1) + t; } }]), e;
    }(), pe = {}, de = !1, ge = null, fe = !0, me = 0;
    function ve(e) {
        if (fe) {
            for (var t, n = f(".sidebar"), r = k(".anchor"), i = y(n, ".sidebar-nav"), o = y(n, "li.active"), a = document.documentElement, s = (a && a.scrollTop || document.body.scrollTop) - me, l = 0, c = r.length; l < c; l += 1) {
                var u = r[l];
                if (u.offsetTop > s) {
                    t || (t = u);
                    break;
                }
                t = u;
            }
            if (t) {
                var h = pe[be(e, t.getAttribute("data-id"))];
                if (h && h !== o && (o && o.classList.remove("active"), h.classList.add("active"), o = h, !de && v.classList.contains("sticky"))) {
                    var p = n.clientHeight, d = o.offsetTop + o.clientHeight + 40, g = d - 0 < p, m = o.offsetTop >= i.scrollTop && d <= i.scrollTop + p ? i.scrollTop : g ? 0 : d - p;
                    n.scrollTop = m;
                }
            }
        }
    }
    function be(e, t) { return e + "?id=" + t; }
    function ye(e, t) {
        if (t) {
            var n = y("#" + t);
            n && (r = n, ge && ge.stop(), fe = !1, ge = new he({ start: window.pageYOffset, end: r.getBoundingClientRect().top + window.pageYOffset, duration: 500 }).on("tick", function (e) { return window.scrollTo(0, e); }).on("done", function () { fe = !0, ge = null; }).begin());
            var r, i = pe[be(e, t)], o = y(f(".sidebar"), "li.active");
            o && o.classList.remove("active"), i && i.classList.add("active");
        }
    }
    var ke = m.scrollingElement || m.documentElement;
    var we = {};
    function xe(e, t) {
        var r = e.compiler, i = e.raw;
        void 0 === i && (i = "");
        var o, a = e.fetch;
        if (o = we[i])
            return t(o);
        var s = r._marked, l = s.lexer(i), c = [], u = s.InlineLexer.rules.link, h = l.links;
        l.forEach(function (e, t) { "paragraph" === e.type && (e.text = e.text.replace(new RegExp(u.source, "g"), function (e, n, i, o) { var a = r.compileEmbed(i, o); return a ? ("markdown" !== a.type && "code" !== a.type || c.push({ index: t, embed: a }), a.code) : e; })); });
        var p = 0;
        !function e(t, n) {
            var r = t.step;
            void 0 === r && (r = 0);
            var i = t.embedTokens, o = t.compile, a = t.fetch, s = i[r];
            if (!s)
                return n({});
            M(s.embed.url).then(function (t) { var l; t && ("markdown" === s.embed.type ? l = o.lexer(t) : "code" === s.embed.type && (l = o.lexer("```" + s.embed.lang + "\n" + t.replace(/`/g, "@DOCSIFY_QM@") + "\n```\n"))), n({ token: s, embedToken: l }), e({ step: ++r, compile: o, embedTokens: i, fetch: a }, n); });
        }({ compile: s, embedTokens: c, fetch: a }, function (e) {
            var r = e.embedToken, o = e.token;
            if (o) {
                var a = o.index + p;
                n(h, r.links), l = l.slice(0, a).concat(r, l.slice(a + 1)), p += r.length - 1;
            }
            else
                we[i] = l.concat(), l.links = we[i].links = h, t(l);
        });
    }
    function _e() {
        var e = k(".markdown-section>script").filter(function (e) { return !/template/.test(e.type); })[0];
        if (!e)
            return !1;
        var t = e.innerText.trim();
        if (!t)
            return !1;
        setTimeout(function (e) { window.__EXECUTE_RESULT__ = new Function(t)(); }, 0);
    }
    function Se(e, t, n) {
        return t = "function" == typeof n ? n(t) : "string" == typeof n ? function (e) {
            var t = [], n = 0;
            return e.replace(N, function (r, i, o) { t.push(e.substring(n, o - 1)), n = o += r.length + 1, t.push(function (e) { return ("00" + ("string" == typeof q[r] ? e[q[r]]() : q[r](e))).slice(-r.length); }); }), n !== e.length && t.push(e.substring(n)), function (e) {
                for (var n = "", r = 0, i = e || new Date; r < t.length; r++)
                    n += "string" == typeof t[r] ? t[r] : t[r](i);
                return n;
            };
        }(n)(new Date(t)) : t, e.replace(/{docsify-updated}/g, t);
    }
    function Ce(e) { e || (e = "not found"), this._renderTo(".markdown-section", e), !this.config.loadSidebar && this._renderSidebar(), !1 === this.config.executeScript || void 0 === window.Vue || _e() ? this.config.executeScript && _e() : setTimeout(function (e) { var t = window.__EXECUTE_RESULT__; t && t.$destroy && t.$destroy(), window.__EXECUTE_RESULT__ = (new window.Vue).$mount("#main"); }, 0); }
    function Le(e) {
        var t = e.config;
        e.compiler = new ae(t, e.router), h && (window.__current_docsify_compiler__ = e.compiler);
        var n = t.el || "#app", r = y("nav") || w("nav"), i = y(n), o = "", a = v;
        i ? (t.repo && (o += (s = t.repo, s ? (/\/\//.test(s) || (s = "https://github.com/" + s), '<a href="' + (s = s.replace(/^git\+/, "")) + '" class="github-corner" aria-label="View source on Github"><svg viewBox="0 0 250 250" aria-hidden="true"><path d="M0,0 L115,115 L130,115 L142,142 L250,250 L250,0 Z"></path><path d="M128.3,109.0 C113.8,99.7 119.0,89.6 119.0,89.6 C122.0,82.7 120.5,78.6 120.5,78.6 C119.2,72.0 123.4,76.3 123.4,76.3 C127.3,80.9 125.5,87.3 125.5,87.3 C122.9,97.6 130.6,101.9 134.4,103.2" fill="currentColor" style="transform-origin: 130px 106px;" class="octo-arm"></path><path d="M115.0,115.0 C114.9,115.1 118.7,116.5 119.8,115.4 L133.7,101.6 C136.9,99.2 139.9,98.4 142.2,98.6 C133.8,88.0 127.5,74.4 143.8,58.0 C148.5,53.4 154.0,51.2 159.7,51.0 C160.3,49.4 163.2,43.6 171.4,40.1 C171.4,40.1 176.1,42.5 178.8,56.2 C183.1,58.6 187.2,61.8 190.9,65.4 C194.5,69.0 197.7,73.2 200.1,77.6 C213.8,80.2 216.3,84.9 216.3,84.9 C212.7,93.1 206.9,96.0 205.4,96.6 C205.1,102.4 203.0,107.8 198.3,112.5 C181.9,128.9 168.3,122.5 157.7,114.1 C157.9,116.9 156.7,120.9 152.7,124.9 L141.0,136.5 C139.8,137.7 141.6,141.9 141.8,141.8 Z" fill="currentColor" class="octo-body"></path></svg></a>') : "")), t.coverpage && (o += '<section class="cover show" style="background: linear-gradient(to left bottom, hsl(' + Math.floor(255 * Math.random()) + ", 100%, 85%) 0%,hsl(" + Math.floor(255 * Math.random()) + ', 100%, 85%) 100%)"><div class="cover-main">\x3c!--cover--\x3e</div><div class="mask"></div></section>'), o += function (e) { var t = '<button class="sidebar-toggle"><div class="sidebar-toggle-button"><span></span><span></span><span></span></div></button><aside class="sidebar">' + (e.name ? '<h1><a class="app-name-link" data-nosearch>' + e.name + "</a></h1>" : "") + '<div class="sidebar-nav">\x3c!--sidebar--\x3e</div></aside>'; return (p ? t + "<main>" : "<main>" + t) + '<section class="content"><article class="markdown-section" id="main">\x3c!--main--\x3e</article></section></main>'; }(t), e._renderTo(i, o, !0)) : e.rendered = !0;
        var s;
        t.mergeNavbar && p ? a = y(".sidebar") : (r.classList.add("app-nav"), t.repo || r.classList.add("no-badge")), _(a, r), t.themeColor && (m.head.appendChild(w("div", (l = t.themeColor, "<style>:root{--theme-color: " + l + ";}</style>")).firstElementChild), function (e) {
            if (!(window.CSS && window.CSS.supports && window.CSS.supports("(--v:red)"))) {
                var t = k("style:not(.inserted),link");
                [].forEach.call(t, function (t) {
                    if ("STYLE" === t.nodeName)
                        j(t, e);
                    else if ("LINK" === t.nodeName) {
                        var n = t.getAttribute("href");
                        if (!/\.css$/.test(n))
                            return;
                        M(n).then(function (t) { var n = w("style", t); b.appendChild(n), j(n, e); });
                    }
                });
            }
        }(t.themeColor));
        var l;
        e._updateRender(), L(v, "ready");
    }
    var Ee = {};
    var $e = function (e) { this.config = e; };
    $e.prototype.getBasePath = function () { return this.config.basePath; }, $e.prototype.getFile = function (e, t) { void 0 === e && (e = this.getCurrentPath()); var n = this.config, r = this.getBasePath(), i = "string" != typeof n.ext ? ".md" : n.ext; e = n.alias ? function e(t, n, r) { var i = Object.keys(n).filter(function (e) { return (Ee[e] || (Ee[e] = new RegExp("^" + e + "$"))).test(t) && t !== r; })[0]; return i ? e(t.replace(Ee[i], n[i]), n, t) : t; }(e, n.alias) : e, o = e, a = i; var o, a; return e = (e = new RegExp("\\.(" + a.replace(/^\./, "") + "|html)$", "g").test(o) ? o : /\/$/g.test(o) ? o + "README" + a : "" + o + a) === "/README" + i ? n.homepage || e : e, e = K(e) ? e : J(r, e), t && (e = e.replace(new RegExp("^" + r), "")), e; }, $e.prototype.onchange = function (e) { void 0 === e && (e = i), e(); }, $e.prototype.getCurrentPath = function () { }, $e.prototype.normalize = function () { }, $e.prototype.parse = function () { }, $e.prototype.toURL = function (e, t, r) {
        var i = r && "#" === e[0], o = this.parse(ne(e));
        if (o.query = n({}, o.query, t), e = (e = o.path + Z(o.query)).replace(/\.md(\?)|\.md$/, "$1"), i) {
            var a = r.indexOf("?");
            e = (a > 0 ? r.substr(0, a) : r) + e;
        }
        return te("/" + e);
    };
    function Ae(e) { var t = location.href.indexOf("#"); location.replace(location.href.slice(0, t >= 0 ? t : 0) + "#" + e); }
    var Te = function (e) {
        function t(t) { e.call(this, t), this.mode = "hash"; }
        return e && (t.__proto__ = e), t.prototype = Object.create(e && e.prototype), t.prototype.constructor = t, t.prototype.getBasePath = function () { var e = window.location.pathname || "", t = this.config.basePath; return /^(\/|https?:)/g.test(t) ? t : te(e + "/" + t); }, t.prototype.getCurrentPath = function () { var e = location.href, t = e.indexOf("#"); return -1 === t ? "" : e.slice(t + 1); }, t.prototype.onchange = function (e) { void 0 === e && (e = i), S("hashchange", e); }, t.prototype.normalize = function () {
            var e = this.getCurrentPath();
            if ("/" === (e = ne(e)).charAt(0))
                return Ae(e);
            Ae("/" + e);
        }, t.prototype.parse = function (e) { void 0 === e && (e = location.href); var t = "", n = e.indexOf("#"); n >= 0 && (e = e.slice(n + 1)); var r = e.indexOf("?"); return r >= 0 && (t = e.slice(r + 1), e = e.slice(0, r)), { path: e, file: this.getFile(e, !0), query: V(t) }; }, t.prototype.toURL = function (t, n, r) { return "#" + e.prototype.toURL.call(this, t, n, r); }, t;
    }($e), Pe = function (e) {
        function t(t) { e.call(this, t), this.mode = "history"; }
        return e && (t.__proto__ = e), t.prototype = Object.create(e && e.prototype), t.prototype.constructor = t, t.prototype.getCurrentPath = function () { var e = this.getBasePath(), t = window.location.pathname; return e && 0 === t.indexOf(e) && (t = t.slice(e.length)), (t || "/") + window.location.search + window.location.hash; }, t.prototype.onchange = function (e) {
            void 0 === e && (e = i), S("click", function (t) {
                var n = "A" === t.target.tagName ? t.target : t.target.parentNode;
                if ("A" === n.tagName && !/_blank/.test(n.target)) {
                    t.preventDefault();
                    var r = n.href;
                    window.history.pushState({ key: r }, "", r), e();
                }
            }), S("popstate", e);
        }, t.prototype.parse = function (e) { void 0 === e && (e = location.href); var t = "", n = e.indexOf("?"); n >= 0 && (t = e.slice(n + 1), e = e.slice(0, n)); var r = J(location.origin), i = e.indexOf(r); return i > -1 && (e = e.slice(i + r.length)), { path: e, file: this.getFile(e), query: V(t) }; }, t;
    }($e);
    var Oe = {};
    function Fe(e) { e.router.normalize(), e.route = e.router.parse(), v.setAttribute("data-page", e.route.file); }
    function Me(e) { !function (e, t) { var n = function (e) { return v.classList.toggle("close"); }; S(e = f(e), "click", function (e) { e.stopPropagation(), n(); }), p && S(v, "click", function (e) { return v.classList.contains("close") && n(); }); }("button.sidebar-toggle", e.router), t = ".sidebar", e.router, S(t = f(t), "click", function (e) { var t = e.target; "A" === t.nodeName && t.nextSibling && t.nextSibling.classList.contains("app-sub-sidebar") && L(t.parentNode, "collapse"); }); var t; e.config.coverpage ? !p && S("scroll", le) : v.classList.add("sticky"); }
    function je(e, t, n, r, i, o) { e = o ? e : e.replace(/\/$/, ""), (e = ee(e)) && M(i.router.getFile(e + n) + t, !1, i.config.requestHeaders).then(r, function (o) { return je(e, t, n, r, i); }); }
    var Ne = Object.freeze({ cached: e, hyphenate: t, merge: n, isPrimitive: r, noop: i, isFn: o, inBrowser: h, isMobile: p, supportsPushState: d, parseQuery: V, stringifyQuery: Z, getPath: J, isAbsolutePath: K, getParentPath: ee, cleanPath: te, replaceSlug: ne });
    function qe() { this._init(); }
    var Re = qe.prototype;
    Re._init = function () {
        this.config = a || {}, (e = this)._hooks = {}, e._lifecycle = {}, ["init", "mounted", "beforeEach", "afterEach", "doneEach", "ready"].forEach(function (t) { var n = e._hooks[t] = []; e._lifecycle[t] = function (e) { return n.push(e); }; });
        var e;
        [].concat((t = this).config.plugins).forEach(function (e) { return o(e) && e(t._lifecycle, t); });
        var t;
        u(this, "init"), function (e) { var t, n = e.config; t = "history" === (n.routerMode || "hash") && d ? new Pe(n) : new Te(n), e.router = t, Fe(e), Oe = e.route, t.onchange(function (t) { Fe(e), e._updateRender(), Oe.path !== e.route.path ? (e.$fetch(), Oe = e.route) : e.$resetEvents(); }); }(this), Le(this), Me(this), function (e) {
            var t = e.config.loadSidebar;
            if (e.rendered) {
                var n = ce(e.router, ".sidebar-nav", !0, !0);
                t && n && (n.parentNode.innerHTML += window.__SUB_SIDEBAR__), e._bindEventOnRendered(n), e.$resetEvents(), u(e, "doneEach"), u(e, "ready");
            }
            else
                e.$fetch(function (t) { return u(e, "ready"); });
        }(this), u(this, "mounted");
    };
    Re.route = {};
    (He = Re)._renderTo = function (e, t, n) { var r = f(e); r && (r[n ? "outerHTML" : "innerHTML"] = t); }, He._renderSidebar = function (e) { var t = this.config, n = t.maxLevel, r = t.subMaxLevel, i = t.loadSidebar; this._renderTo(".sidebar-nav", this.compiler.sidebar(e, n)); var o = ce(this.router, ".sidebar-nav", !0, !0); i && o ? o.parentNode.innerHTML += this.compiler.subSidebar(r) || "" : this.compiler.subSidebar(), this._bindEventOnRendered(o); }, He._bindEventOnRendered = function (e) {
        var t = this.config, n = t.autoHeader, r = t.auto2top;
        if (function (e) {
            var t = y(".cover.show");
            me = t ? t.offsetHeight : 0;
            for (var n = f(".sidebar"), r = k(n, "li"), i = 0, o = r.length; i < o; i += 1) {
                var a = r[i], s = a.querySelector("a");
                if (s) {
                    var l = s.getAttribute("href");
                    if ("/" !== l) {
                        var c = e.parse(l), u = c.query.id, h = c.path;
                        u && (l = be(h, u));
                    }
                    l && (pe[decodeURIComponent(l)] = a);
                }
            }
            if (!p) {
                var d = e.getCurrentPath();
                C("scroll", function () { return ve(d); }), S("scroll", function () { return ve(d); }), S(n, "mouseover", function () { de = !0; }), S(n, "mouseleave", function () { de = !1; });
            }
        }(this.router), n && e) {
            var i = f("#main"), o = i.children[0];
            if (o && "H1" !== o.tagName) {
                var a = w("h1");
                a.innerText = e.innerText, _(i, a);
            }
        }
        r && (s = r, void 0 === s && (s = 0), ke.scrollTop = !0 === s ? 0 : Number(s));
        var s;
    }, He._renderNav = function (e) { e && this._renderTo("nav", this.compiler.compile(e)), ce(this.router, "nav"); }, He._renderMain = function (e, t, n) {
        var r = this;
        if (void 0 === t && (t = {}), !e)
            return Ce.call(this, e);
        u(this, "beforeEach", e, function (e) { var i, o = function () { t.updatedAt && (i = Se(i, t.updatedAt, r.config.formatUpdated)), u(r, "afterEach", i, function (e) { return Ce.call(r, e); }); }; r.isHTML ? (i = r.result, o(), n()) : xe({ compiler: r.compiler, raw: e }, function (e) { i = r.compiler.compile(e), o(), n(); }); });
    }, He._renderCover = function (e, t) {
        var n = f(".cover");
        if (L(f("main"), t ? "add" : "remove", "hidden"), e) {
            L(n, "add", "show");
            var r = this.coverIsHTML ? e : this.compiler.cover(e), i = r.trim().match('<p><img.*?data-origin="(.*?)"[^a]+alt="(.*?)">([^<]*?)</p>$');
            if (i) {
                if ("color" === i[2])
                    n.style.background = i[1] + (i[3] || "");
                else {
                    var o = i[1];
                    L(n, "add", "has-mask"), K(i[1]) || (o = J(this.router.getBasePath(), i[1])), n.style.backgroundImage = "url(" + o + ")", n.style.backgroundSize = "cover", n.style.backgroundPosition = "center center";
                }
                r = r.replace(i[0], "");
            }
            this._renderTo(".cover-main", r), le();
        }
        else
            L(n, "remove", "show");
    }, He._updateRender = function () {
        !function (e) {
            var t = f(".app-name-link"), n = e.config.nameLink, i = e.route.path;
            if (t)
                if (r(e.config.nameLink))
                    t.setAttribute("href", n);
                else if ("object" == typeof n) {
                    var o = Object.keys(n).filter(function (e) { return i.indexOf(e) > -1; })[0];
                    t.setAttribute("href", n[o]);
                }
        }(this);
    };
    var He;
    !function (e) {
        var t;
        e._fetch = function (e) {
            var n = this;
            void 0 === e && (e = i);
            var r = this.route, o = r.path, a = Z(r.query, ["id"]), s = this.config, l = s.loadNavbar, c = s.loadSidebar, u = s.requestHeaders;
            t && t.abort && t.abort(), t = M(this.router.getFile(o) + a, !0, u), this.isHTML = /\.html$/g.test(o);
            var h = function () {
                if (!c)
                    return e();
                je(o, a, c, function (t) { n._renderSidebar(t), e(); }, n, !0);
            };
            t.then(function (e, t) { n._renderMain(e, t, h); }, function (e) { n._renderMain(null, {}, h); }), l && je(o, a, l, function (e) { return n._renderNav(e); }, this, !0);
        }, e._fetchCover = function () {
            var e = this, t = this.config, n = t.coverpage, r = t.requestHeaders, i = this.route.query, o = ee(this.route.path);
            if (n) {
                var a = null, s = this.route.path;
                if ("string" == typeof n)
                    "/" === s && (a = n);
                else if (Array.isArray(n))
                    a = n.indexOf(s) > -1 && "_coverpage";
                else {
                    var l = n[s];
                    a = !0 === l ? "_coverpage" : l;
                }
                var c = !!a && this.config.onlyCover;
                return a ? (a = this.router.getFile(o + a), this.coverIsHTML = /\.html$/g.test(a), M(a + Z(i, ["id"]), !1, r).then(function (t) { return e._renderCover(t, c); })) : this._renderCover(null, c), c;
            }
        }, e.$fetch = function (e) { var t = this; void 0 === e && (e = i); var n = function () { u(t, "doneEach"), e(); }; this._fetchCover() ? n() : this._fetch(function (e) { t.$resetEvents(), n(); }); };
    }(Re), Re.$resetEvents = function () { ye(this.route.path, this.route.query.id), ce(this.router, "nav"); };
    window.Docsify = { util: Ne, dom: E, get: M, slugify: W }, window.DocsifyCompiler = ae, window.marked = z, window.Prism = I, qe.version = "4.6.3", function (e) {
        var t = document.readyState;
        if ("complete" === t || "interactive" === t)
            return setTimeout(e, 0);
        document.addEventListener("DOMContentLoaded", e);
    }(function (e) { return new qe; });
}();
