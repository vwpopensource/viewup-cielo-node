"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class CieloError {
    /**
     * @memberOf {CieloAPIFeedback}
     * @param {string} message
     * @param {number} code
     */
    constructor(message, code) {
        this.code = code;
        this.message = this.getMessage();
    }
    getMessage() {
        if (!CieloError.Locale) {
            return this.message;
        }
        const finder = (error) => String(error.code) === String(this.code);
        const feedback = CieloError.Locale.errors.find(finder) || CieloError.Locale.updates.find(finder) || { message: this.message };
        return feedback.message;
    }
    static register(Locale) {
        CieloError.Locale = Locale;
    }
}
exports.default = CieloError;
